# About
Lai projektu apskatītu pārlūkā:
 - `mkdir auction && cd $_`
 - `git clone https://gitlab.com/animaacija/mikrotik-auction-app.git .`
 - `docker-compose up`
 - iespējams, jāuzgaida kādas 5 minūtes, kamēr viss iesāk darboties
 - tad `open http://0.0.0.0:8080`

Realizēt uzdevuma punktus, jeb izpildot pašu minimumu, rezultātu var sasniegt ļoti ātri un samērā vienkārši.

Tāpēc, nolēmu gatavot šo mājasdarbu kā prezentāciju - kā iespēju man parādīt, ka, piemēram, `Trait` un `Interface` man nav svešums. Jauku lasīšanu!

Ir piedomāts, par gadījumu, ja klients ir atspējojis pārlūkā javascript, vai pat ja pārlūks nevalidē HTML5 formu - arī tad klientam tiks parādītas formas validācijas kļūdas. Pretējā gadījumā validācija un datu sūtīšana notiek caur javascript.

Konteksti:
 - Accessibility: web lapai jābūt "keyboard" interaktīvai.
 - Architecture: reizē ar šo aplikāciju ir tapis "Micro" frameworks `./src/app/`.
 - Logging: konceptuāli ieviesu `./data-logs/access.log`. 
 - 404: Atbilde par neatrastu lapu nāk no php, nevis servera konfigurācijas. 
 - Tests: eksistē testi, pagaidām tikai svarīgākie.
 - Api: uzturēt protokolu (konvenci) starp serveri un api metodēm, lai būtu vieglāks javascript.
 - Security: APP_ENV development.
 - Composer: tikai `twig` - tīrākam php.
 - Documentation: Konceptuāli tāda eksistē - skatīt zemāk.

------
 

# Development

Routes file: `./src/app/routes.php`

Configuration file: `./src/app/conf.php`

```bash
# to view exception messages, export APP_ENV=development
APP_ENV=development docker-compose up -d && docker logs composer -f

# run this to clear app cache
composer fresh

# runs tests
composer test

# run tests and view coverage
composer test-cover

# served at:
open http://0.0.0.0:8080
```

# Contribute
- We use PSR-2
- test all things

# Docker usage
```bash
# if initial project start,
# containers will take time to do `composer install`

# start infrastructure (production)
docker-compose up -d

# cli access database
docker exec -it auction-mysql mysql -uroot -Dexample

# reset all
docker system prune -a
```

