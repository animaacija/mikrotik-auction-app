<?php declare(strict_types=1);

namespace Auction;

use InputValidator;

/**
 * Class AuctionItem
 * @package Auction
 */
class AuctionItem
{
    /**
     * @var array
     */
    protected static $validationRules = [
        'year_of_manufacture' => [
            InputValidator::REQUIRED => true,
            InputValidator::CAST_INT => true,
            InputValidator::RULE_GREATER_THAN => 0,
        ],
        'title' => [
            InputValidator::REQUIRED => true,
            InputValidator::CAST_STRING => true,
            InputValidator::RULE_STRING_MAX_LEN => 128,
        ],
        'initial_price' => [
            InputValidator::REQUIRED => true,
            InputValidator::CAST_FLOAT => true,
            InputValidator::RULE_FLOAT_PRECISION => 2,
            InputValidator::CAST_MULTIPLY => 100,
            InputValidator::CAST_INT => true,
            InputValidator::RULE_GREATER_THAN => 0,
        ],
        'parameters' => [
            InputValidator::REQUIRED => false,
            InputValidator::ARRAY_VALIDATOR => [
                'name' => [
                    InputValidator::REQUIRED => true,
                    InputValidator::CAST_STRING => true,
                    InputValidator::RULE_STRING_MAX_LEN => 128,
                ],
                'value' => [
                    InputValidator::REQUIRED => true,
                    InputValidator::CAST_STRING => true,
                    InputValidator::RULE_STRING_MAX_LEN => 128,
                ],
            ]
        ]
    ];

    /**
     * @param array $fields
     * @return array
     */
    protected static function validate(array &$fields): array
    {
        $validator = new InputValidator(self::$validationRules);

        return $validator->validate($fields);
    }

    /**
     * @param array $parameters
     * @param int   $itemId
     * @return array
     * @throws \Exception
     */
    private static function storeParameters(array $parameters, int $itemId): array
    {
        $qry = 'INSERT INTO `auction_item_parameters` (`item_id`,`name`,`value`) VALUES ';

        $valuesQry = [];
        $fieldsQry = [];
        foreach ($parameters as $index => $param) {
            $valuesQry[] = "(:item_id_{$index}, :name_{$index}, :value_{$index})";

            $fieldsQry["item_id_{$index}"] = $itemId;
            $fieldsQry["name_{$index}"] = $param['name'];
            $fieldsQry["value_{$index}"] = $param['value'];
        }

        $qry .= implode(',', $valuesQry);

        $insertions = db()->mutate($qry, $fieldsQry);

        if ($insertions !== count($parameters)) {
            return [
                'Error happened while creating parameters.',
            ];
        }

        return [];
    }

    /**
     * @param array $fields
     * @return array
     * @throws \Exception
     */
    public static function create(array $fields): array
    {
        $errors = self::validate($fields);

        if ($errors) {
            return $errors;
        }

        $parameters = null;
        if (isset($fields['parameters'])) {
            $parameters = $fields['parameters'];
            unset($fields['parameters']);
        }

        $qry = 'INSERT INTO `auction_items` (
          year_of_manufacture,
          title,
          initial_price
        ) VALUES (
          :year_of_manufacture,
          :title,
          :initial_price
        );';

        $auctionItemId = db()->insert($qry, $fields);
        if (!$auctionItemId) {
            return [
                'Error while creating auction item.'
            ];
        }

        if ($parameters) {
            return self::storeParameters($parameters, $auctionItemId);
        }

        return [];
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public static function delete(int $id): bool
    {
        $qry = 'DELETE from `auction_items` WHERE `id` = :id;';

        return db()->mutate($qry, compact('id')) > 0;
    }

    /**
     * queries db for all auction items with their parameters
     * wraps them in "instances".
     *
     * @return array
     * @throws \Exception
     */
    public static function all(): array
    {
        $collection = [];

        $qry = 'SELECT
                  ai.id, 
                  ai.title, 
                  ai.initial_price, 
                  ai.year_of_manufacture, 
                  ai.created_at, 
                  aip.name parameter_name,
                  aip.value parameter_value
                FROM `auction_items` `ai`
                LEFT JOIN `auction_item_parameters` aip ON aip.item_id = ai.id
                ORDER BY ai.created_at DESC;';

        $stmt = db()->stmt($qry);

        while ($res = $stmt->fetch()) {
            if (!isset($collection[$res->id])) {
                $collection[$res->id] = [
                    'id' => $res->id,
                    'title' => $res->title,
                    'initialPriceEur' => number_format($res->initial_price / 100, 2, ',', ''),
                    'yearOfManufacture' => $res->year_of_manufacture,
                    'createdAt' => $res->created_at,
                    'parameters' => [],
                ];
            }

            if ($res->parameter_name && $res->parameter_value) {
                $collection[$res->id]['parameters'][] = [
                    'name' => $res->parameter_name,
                    'value' => $res->parameter_value,
                ];
            }
        }

        return $collection;
    }
}
