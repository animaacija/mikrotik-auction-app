<?php declare(strict_types=1);

namespace Auction;

use Controller;
use Response;

/**
 * Class HomeController
 * @package Auction
 */
class HomeController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        return Response::redirect('/auction/items');
    }
}
