<?php declare(strict_types=1);

namespace Auction;

use App;
use Controller;
use Response;

/**
 * Class AuctionItemController
 */
class DataController extends Controller
{
    /**
     * @return Response
     * @throws \Exception
     */
    public function index()
    {
        $auctionItems = AuctionItem::all();
        $oldInput = App::getFlashedInput();

        $year = (int) date('Y');
        $years = range($year - 14, $year);

        return Response::view(
            'auction_items_home.html',
            compact('auctionItems', 'oldInput', 'years')
        );
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function store(): Response
    {
        $input = App::getRequest()->all();
        $errors = AuctionItem::create($input);

        if (App::expectsJson()) {
            return Response::json(compact('errors'));
        }

        if ($errors) {
            $errorsKeyed = array_combine(array_column($errors, 'key'), $errors);
            App::flashInput($input + compact('errorsKeyed'));
        } else {
            App::flushInput();
        }

        return $this->index(); // pseudo-redirect
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function delete()
    {
        $id = (int) App::getRequest()->get('id');
        if (!$id) {
            return Response::notFound();
        }

        if (AuctionItem::delete($id)) {
            return Response::json([]);
        }

        return Response::json([
            'errors' => ['Nothing was deleted.']
        ]);
    }
}
