<?php declare(strict_types=1);

/**
 * Class ValidatorTest
 */
class ValidatorTest extends BaseCase
{
    const FIELD_KEY = 'any';

    public function test_cast_int_greater_than()
    {
        list($fieldValue, $errors) = $this->validateField('-11', [
            InputValidator::REQUIRED => true,
            InputValidator::CAST_INT => true,
            InputValidator::RULE_GREATER_THAN => 0,
        ]);

        $this->assertEquals($fieldValue, -11);
        $this->assertEquals($errors, [InputValidator::RULE_GREATER_THAN]);
    }

    public function test_cast_string_max_len()
    {
        list($fieldValue, $errors) = $this->validateField('123456789', [
            InputValidator::REQUIRED => true,
            InputValidator::CAST_STRING => true,
            InputValidator::RULE_STRING_MAX_LEN => 7,
        ]);

        $this->assertEquals($fieldValue, '123456789');
        $this->assertEquals($errors, [InputValidator::RULE_STRING_MAX_LEN]);
    }

    public function test_cast_float_precision()
    {
        list($fieldValue, $errors) = $this->validateField('1.123', [
            InputValidator::REQUIRED => true,
            InputValidator::CAST_FLOAT => true,
            InputValidator::RULE_FLOAT_PRECISION => 2,
        ]);

        $this->assertEquals($fieldValue, 1.123);
        $this->assertEquals($errors, [InputValidator::RULE_FLOAT_PRECISION]);
    }

    public function test_no_errors_cases()
    {
        $dataSet = [
            '1.12' => [
                InputValidator::REQUIRED => true,
                InputValidator::CAST_FLOAT => true,
                InputValidator::RULE_FLOAT_PRECISION => 2,
                InputValidator::RULE_GREATER_THAN => 1,
            ],
            '1.13' => [
                InputValidator::REQUIRED => true,
                InputValidator::CAST_STRING => true,
                InputValidator::RULE_STRING_MAX_LEN => 4,
            ],
        ];

        foreach ($dataSet as $fieldValue => $fieldRules) {
            list(, $errors) = $this->validateField($fieldValue, $fieldRules);
            $this->assertEmpty($errors);
        }

    }

    public function test_nested_validation()
    {
        $rules = [
            InputValidator::ARRAY_VALIDATOR => [
                'float' => [
                    InputValidator::CAST_FLOAT => true,
                    InputValidator::RULE_FLOAT_PRECISION => 2,
                ],
                'string' => [
                    InputValidator::CAST_STRING => true,
                    InputValidator::RULE_STRING_MAX_LEN => 2,
                ],
            ]
        ];

        $value = [
            [
                'float' => 0.122, // to fixed 2 fail
                'string' => 0.221, // too long if string
            ],
            [
                'float' => 0.1,
                'string' => 'ok',
            ],
            [
                'float' => 0.122,  // to fixed 2 fail
                'string' => 'too long',  // too long if string
            ],
        ];

        $expectedErrorKeys = [
            self::FIELD_KEY . '[0][float]',
            self::FIELD_KEY . '[0][string]',
            self::FIELD_KEY . '[2][float]',
            self::FIELD_KEY . '[2][string]',
        ];

        list(,, $errorsKeys) = $this->validateField($value, $rules);
        sort($errorsKeys);
        sort($expectedErrorKeys);

        $this->assertEquals($expectedErrorKeys, $errorsKeys);
    }

    /**
     * Performs a single field validation by given rules
     *
     * @param       $fieldValue
     * @param array $fieldRules
     * @return array
     */
    private function validateField($fieldValue, array $fieldRules): array
    {
        $validator = new InputValidator([
            self::FIELD_KEY => $fieldRules
        ]);

        $fields = [
            self::FIELD_KEY => $fieldValue
        ];

        $validator->validate($fields);

        $errors = array_column($validator->getErrors(), 'id');
        $errorKeys = array_column($validator->getErrors(), 'key');

        return [
            $fields[self::FIELD_KEY],
            $errors,
            $errorKeys
        ];
    }
}
