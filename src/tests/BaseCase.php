<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class BaseCase extends TestCase {

    /**
     * @var array
     */
    public $headers = [];

    /**
     * @var array
     */
    public $input = [];

    /**
     * @var string
     */
    public $uri = '';

    /**
     * @param array $headers
     * @return BaseCase
     */
    public function spoofHeaders(array $headers): BaseCase
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param array $input
     * @return BaseCase
     */
    public function spoofInput(array $input): BaseCase
    {
        $this->input = $input;

        return $this;
    }

    /**
     * @param string $uri
     * @return BaseCase
     */
    public function spoofUri(string $uri): BaseCase
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @param null|string $uri
     * @return SpoofedRequest
     * @throws Exception
     */
    public function spoofRequest(?string $uri = null): SpoofedRequest
    {
        return new SpoofedRequest(
            $uri ?? $this->uri,
            $this->headers,
            $this->input
        );
    }

    /**
     * @return App
     * @throws Exception
     */
    public function spoofApp(): App
    {
        return new App($this->spoofRequest(...func_get_args()));
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function spoofResponse(): Response
    {
        return $this->spoofApp()->getResponse();
    }
}
