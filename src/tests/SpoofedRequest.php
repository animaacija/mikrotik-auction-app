<?php declare(strict_types=1);

class SpoofedRequest extends Request
{
    /**
     * SpoofedRequest constructor.
     * @param string $uri
     * @param array  $headers
     * @param array  $input
     * @throws Exception
     */
    public function __construct(
        string $uri = '',
        array $headers = [],
        array $input = []
    ) {
        $_SERVER = [
            'REQUEST_URI' => $uri,
        ] + $_SERVER;

        $_REQUEST = $input;

        parent::__construct($headers);
    }
}
