<?php declare(strict_types=1);


class RequestTest extends BaseCase
{
    public function test_it_trims_trailing_slashes()
    {
        $uri = 'path/to';
        $request = $this->spoofRequest("{$uri}////");

        $this->assertEquals($uri, $request->getRoute());
    }

    public function test_it_expects_json_verbose()
    {
        $request = $this->spoofHeaders([
            'Content-type' => 'app/json',
        ])->spoofRequest();

        $this->assertTrue($request->getExpectsJSON());
    }

    public function test_it_expects_json_case_xhr()
    {
        $request = $this->spoofHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
        ])->spoofRequest();

        $this->assertTrue($request->getExpectsJSON());
    }

    public function test_it_not_expects_json()
    {
        $request = $this->spoofHeaders([
            'Content-type' => 'app/text',
        ])->spoofRequest();

        $this->assertFalse($request->getExpectsJSON());
    }

    public function test_all_input()
    {
        $input = ['test' => true, 'is' => 'type', 'float' => 0.1];
        $request = $this->spoofInput($input)->spoofRequest();

        $this->assertEquals($input['float'], $request->get('float'));
        $this->assertEquals($input, $request->all());
    }
}
