<?php declare(strict_types=1);


class AppTest extends BaseCase
{
    public function test_it_redirects_with_status_code()
    {
        $response = $this->spoofUri('/')->spoofResponse();

        $this->assertEquals(302, $response->getCode());
        $this->assertEquals(['Location' => '/auction/items',], $response->getHeaders());
    }

    public function test_it_renders_to_string()
    {
        $response = $this->spoofUri('not/found')->spoofResponse();

        $this->assertStringStartsWith('<', $response->render());
    }

}
