<?php declare(strict_types=1);

class SpoofedApp extends App
{

    public function __construct(SpoofedRequest $request) {
        parent::__construct($request);
    }

    public function getResponse(): Response
    {

    }
}
