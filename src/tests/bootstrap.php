<?php

if (!defined('PHPUNIT_RUNNING')) {
    return;
}

if (!defined('BASE_PATH')) {
    define('BASE_PATH', realpath('./'));
}

require_once realpath('vendor/autoload.php');
