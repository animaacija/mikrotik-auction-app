<?php

define('DEV_ENV', getenv('APP_ENV') === 'development');
define('BASE_PATH', realpath('../'));

ini_set('display_errors', DEV_ENV);
ini_set('error_reporting', E_ALL);

require realpath('../vendor/autoload.php');

$request = new Request(getallheaders());
$app = new App($request);

echo $app->render();
