;/* <- good practice to start with semicolon.
*
* EcmaScript-5 safe code here.
* (+) sign and any other unary operator forces this function to be IIFE
* instance is exposed onto 'window' object namespace (only to illustrate easy development)
* */
+function(w, $) {

  /**
   * A simple protocol:
   * - server responds with array of errors in case of any type error
   * - server does not care about request method
   *
   * This factory just toggles "loading" states
   * and displays error messages.
   *
   * @param url
   * @param data
   * @param $loading
   * @param onSuccess
   * @param onError
   */
  function apiProtocol(url, data, $loading, onSuccess, onError) {
    onError = onError || function (msg) {
      App.message(msg);
    };

    function always() {
      $loading.removeClass('loading')
    }

    function done(resp) {
      if (typeof resp === 'string') {
        return fail()
      }
      if (resp.errors && resp.errors.length) {
        return resp.errors.forEach(onError);
      }
      onSuccess(resp)
    }

    function fail() {
      App.message('Network error.');
    }

    $loading.addClass('loading');
    $.post(url || '/', data || {}).always(always).done(done).fail(fail)
  }

  /**
   * Main object
   *
   * @param $table
   * @param $form
   * @param $messages
   * @constructor
   */
  function App($table, $form, $messages) {
    this.table = new AuctionTable($table);
    this.form = new AuctionForm($form);
    this.$messages = $messages
  }

  /**
   *
   * @param msg
   */
  App.prototype.message = function (msg) {
    var $node = $('<div/>', {text: msg, class: 'toast'});
    this.$messages.prepend($node);

    $node.on('click', function () {
      $node.remove()
    });

    setTimeout(function () {
      $node.click()
    }, 4000)
  };

  /**
   * Table object
   *
   * @param $table
   * @constructor
   */
  function AuctionTable($table) {
    this.ref = $table;
    this.$btnsDelete = $table.find('[data-action="delete"]', 'button');

    var that = this;
    this.$btnsDelete.on('click', function () {
      confirm('Are you sure?') && that.deleteItem($(this))
    });
  }

  /**
   *
   * @param $button
   */
  AuctionTable.prototype.deleteItem = function ($button) {
    function onSuccess () {
      $button.closest('[data-item-row]').remove()
    }

    apiProtocol(
      '/api/auction/items/delete',
      $button.data(),
      $button,
      onSuccess
    );
  };

  /**
   * Form object.
   * If javascript in browser is disabled - will do post.
   * Will validate before post
   *
   * @param $form
   * @constructor
   */
  function AuctionForm($form) {
    this.$form = $form;
    // caching few DOM queries
    this.$form.$inpTitle = this.$form.find('[name="title"]');
    this.$form.$inpYear = this.$form.find('[name="year_of_manufacture"]');
    this.$form.$inpPrice = this.$form.find('[name="initial_price"]');

    // Usually it's: `$form.on('submit', e => e.preventDefault() && validate())`
    // instead we "take over" button,
    // this way html validation will work if js is disabled
    this.$btnSubmit = $form.find('[type="submit"]', 'button');
    this.$btnSubmit.attr('type', 'button');

    this.$parametersContainer = $form.find('#parameters');
    this.parameters = [];
    this.$btnAddParameter = $form.find('#addParameter', 'button');

    this.$btnAddParameter.on('click', this.addParameter.bind(this));
    this.$btnSubmit.on('click', this.submit.bind(this));

    if (!this.$parametersContainer.length) {
      this.$btnAddParameter.attr('disabled', true);
      throw new Error("AuctionForm #parameters not found.")
    }
  }

  /**
   *
   */
  AuctionForm.prototype.removeErrors = function() {
    this.$form.find('.error').remove();
  };

  /**
   * sends out this form
   */
  AuctionForm.prototype.send = function() {
    this.removeErrors();
    apiProtocol(
      '/api/auction/items/store',
      this.$form.serializeArray(),
      this.$btnSubmit,
      function () {
        App.message("Success!");
        w.location.reload()
      },
      this.displayError.bind(this)
    );
  };

  /**
   * @param err
   */
  AuctionForm.prototype.displayError = function(err) {
    var errNode = w.document.createElement('div');

    errNode.classList.add('error');
    errNode.classList.add('form-row');
    errNode.textContent = err.reason;

    this.$form.find('[name="' + err.key + '"]')
      .closest('.form-row')
      .before(errNode)
  };

  /**
   * return array of found error objects
   */
  AuctionForm.prototype.validate = function() {
    var errors = [];

    ruleRequired.message = "Required.";
    function ruleRequired(val) {
      return !! val;
    }

    ruleLength.message = "Must not exceed 100 characters.";
    function ruleLength(val) {
      return val.length <= 100;
    }

    ruleGtZero.message = "Must me greater than zero.";
    function ruleGtZero(val) {
      return val > 0;
    }

    ruleFloating.message = "Two floating point precision limit.";
    function ruleFloating(val) {
      val = parseFloat(val);
      return parseFloat(val.toFixed(2)) >= val;
    }

    function runRules($inp, rules) {
      var val = $inp.val();
      while (cb = rules.shift()) {
        if (!cb(val)) {
          errors.push({
            key: $inp.attr('name'),
            reason: cb.message,
          });
          break;
        }
      }
    }

    function runParameterRules(param) {
      runRules(param.$node.$inpName, [ruleRequired, ruleLength]);
      runRules(param.$node.$inpValue, [ruleRequired, ruleLength]);
    }

    this.parameters.forEach(runParameterRules);

    runRules(this.$form.$inpTitle, [ruleRequired, ruleLength]);
    runRules(this.$form.$inpYear, [ruleRequired, ruleGtZero]);
    runRules(this.$form.$inpPrice, [ruleRequired, ruleGtZero, ruleFloating]);

    return errors;
  };

  /**
   *
   */
  AuctionForm.prototype.submit = function() {
    var errors = this.validate();
    if (errors.length) {
      this.removeErrors();
      errors.forEach(this.displayError.bind(this))
    } else {
      this.send();
    }
  };

  /**
   * adds next field
   */
  AuctionForm.prototype.addParameter = function() {
    var parameter = new AuctionFormParameter(this.parameters.length + 1);
    this.parameters.push(parameter);
    this.$parametersContainer.append(parameter.$node)
  };

  /**
   * ItemParameter object
   *
   * @param id
   * @constructor
   */
  function AuctionFormParameter(id) {
    this.$node = this.createNode(id)
  }

  /**
   * For sure, vanilla javascript would perform faster,
   * using jQuery only because so it's stated in task description.
   *
   * @param id
   * @returns {jQuery|HTMLElement}
   */
  AuctionFormParameter.prototype.createNode = function (id) {
    var $node = $('<div />', {class: 'form-row'});

    var nameId = 'parameterName' + id;
    var valueId = 'parameterValue' + id;

    var $name = $('<div />', {class: 'form-row'});
    var $value = $('<div />', {class: 'form-row'});

    $name.append($('<label />', {for: nameId, text: 'Name'}));
    $node.$inpName = $('<input />', {id: nameId, type: 'text', name: 'parameters[' + id + '][name]'});
    $name.append($node.$inpName);

    $value.append($('<label />', {for: valueId, text: 'Value'}));
    $node.$inpValue = $('<input />', {id: valueId, type: 'text', name: 'parameters[' + id + '][value]'});
    $value.append($node.$inpValue);

    $node.$removeBtn = $('<button />', {text: 'remove', class: 'btn-small'});

    $node.append([$name, $value]);
    $node.append($node.$removeBtn);

    $node.$removeBtn.on('click', this.remove.bind(this));

    return $node;
  };

  /**
   * Removes DOM node.
   * jQuery will automatically remove event handlers.
   */
  AuctionFormParameter.prototype.remove = function () {
    this.$node.prev('.error.form-row').remove();
    this.$node.remove();
  };

  /**
   * On DOM ready callback.
   */
  $(function () {
    $('#no-js-msg').remove();

    var app = new App(
      $('table'),
      $('form'),
      $('#messages')
    );

    App.message = app.message.bind(app);
    w.app = app
  })
}(window, jQuery);

