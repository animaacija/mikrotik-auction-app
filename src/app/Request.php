<?php declare(strict_types=1);

/**
 * Class Request
 *
 * Currently App does not care about request method (DELETE,PATCH,GET...)
 * that's why there is no implementation for it.
 */
class Request
{

    /**
     * @var array
     */
    protected $input;

    /**
     * @var array
     */
    protected $headers;

    /**
     * Common convention is used if client expects json response
     * if header "Content-Type" or "Accepts" has "* /json".
     *
     * @var bool
     */
    protected $expectsJSON;

    /**
     * @var string
     */
    protected $route;

    /**
     * Request constructor.
     * @param array $headers
     */
    public function __construct(array $headers)
    {
        $this->route = rtrim($_SERVER['REQUEST_URI'] ?? '', '/');
        $this->headers = $headers;

        $this->expectsJSON = $this->guessIfExpectsJSON();
    }

    /**
     * conventional alias
     *
     * @return array
     */
    public function all(): array
    {
        return $_REQUEST ?? [];
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return $_REQUEST[$key] ?? null;
    }

    /**
     * @return bool
     */
    public function getExpectsJSON(): bool
    {
        return $this->expectsJSON;
    }


    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return bool
     */
    private function guessIfExpectsJSON(): bool
    {
        $jsLibHeader = $this->headers['X-Requested-With'] ?? '';
        if ($jsLibHeader === 'XMLHttpRequest') {
            return true;
        }

        $lookup = ($this->headers['Content-type'] ?? '') . ($this->headers['Accept'] ?? '');
        $match = preg_match(
            '/.*?\/json/',
            $lookup
        );

        if ($match) {
            return true;
        }

        return false;
    }
}
