<?php declare(strict_types=1);

/**
 * Class Database
 */
class Database
{
    /**
     * @var PDO
     */
    protected $pdo;

    /**
     * Database constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $dbConf = conf('db');

        $dsn = "mysql:host={$dbConf['host']};dbname={$dbConf['name']};charset=utf8";
        $opt = [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $pdo = new PDO($dsn, $dbConf['user'], $dbConf['pass'], $opt);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        $this->pdo = $pdo;
    }

    /**
     * @param string $qry
     * @param array  $bindings
     * @return PDOStatement
     */
    public function stmt(string $qry, array $bindings = []): PDOStatement
    {
        $stmt = $this->pdo->prepare($qry);
        $stmt->execute($bindings);

        return $stmt;
    }

    /**
     * @param string $qry
     * @param array  $bindings
     * @return array
     */
    public function query(string $qry, array $bindings = []): array
    {
        return $this->stmt($qry, $bindings)->fetchAll();
    }

    /**
     * @param string $qry
     * @param array  $bindings
     * @return int last insert id
     */
    public function insert(string $qry, array $bindings = []): ?int
    {
        if ($this->mutate($qry, $bindings)) {
            return (int) $this->pdo->lastInsertId();
        }

        return null;
    }

    /**
     * @param string $qry
     * @param array  $bindings
     * @return int
     */
    public function mutate(string $qry, array $bindings = []): int
    {
        return $this->stmt($qry, $bindings)->rowCount();
    }
}
