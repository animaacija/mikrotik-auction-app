<?php declare(strict_types=1);

/**
 * Class Controller also serves as "Router"
 */
class Controller {

    /**
     * @var string[]
     */
    protected static $routes;

    /**
     * invokes routed method
     * writes to response object
     *
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    final public function processRequest(Request $request): Response
    {
        $route = static::$routes[$request->getRoute()] ?? null;

        if (!$route) {
            return Response::notFound();
        }

        $callable = $this->getRoutedMethod($route);

        return $callable();
    }

    /**
     * @param string $routedMethodName
     * @return callable
     * @throws Exception
     */
    final protected function getRoutedMethod(string $routedMethodName): callable
    {
        $delimited = explode('::', $routedMethodName);
        if (count($delimited) != 2) {
            throw new Exception("{$routedMethodName} is not valid.");
        }

        list($class, $method) = $delimited;
        if (!class_exists($class)) {
            throw new Exception("Class: {$class} not found.");
        }

        $instance = new $class;
        if (!method_exists($instance, $method)) {
            throw new Exception("Method: {$method} not implemented in {$class}.");
        }

        return [$instance, $method];
    }

    /**
     * @param string $path
     * @param string $method
     */
    final static function bindRoute(string $path, string $method)
    {
        self::$routes[$path] = $method;
    }
}
