<?php

if (!function_exists('conf')) {
    /**
     * loads configuration into static variable
     * to be read via this helper
     *
     * @param string|null $key
     * @return mixed
     * @throws Exception
     */
    function conf(string $key) {
        static $conf;

        if (!$conf) {
            $conf = require __DIR__ . '/conf.php';
        }

        if ($conf && array_key_exists($key, $conf)) {
            return $conf[$key];
        }

        throw new Exception("Reading nonexistent config key: {$key}");
    }
}

if (!function_exists('db')) {

    /**
     * constructs connection on first call only
     * then serves it statically
     *
     * @return Database
     * @throws Exception
     */
    function db() {
        static $db;

        if (!$db) {
            $db = new Database;
        }

        return $db;
    }
}
