<?php declare(strict_types=1);

/**
 * Class JsonRender
 */
class JsonRender implements RendererInterface
{
    use RendererTrait;

    /**
     * @return string
     */
    public function render(): string
    {
        return json_encode($this->getData());
    }

}
