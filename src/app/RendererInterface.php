<?php declare(strict_types=1);

/**
 * Interface RendererInterface
 */
interface RendererInterface
{
    /**
     * @return string
     */
    public function render(): string;

    /**
     * @return array
     */
    public function getData(): array;

    /**
     * @param array $data
     * @return mixed
     */
    public function setData(array $data);
}
