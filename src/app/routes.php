<?php

Controller::bindRoute('', 'Auction\HomeController::index');
Controller::bindRoute('/auction/items', 'Auction\DataController::index');
Controller::bindRoute('/auction/items/store', 'Auction\DataController::store');
Controller::bindRoute('/api/auction/items/store', 'Auction\DataController::store');
Controller::bindRoute('/api/auction/items/delete', 'Auction\DataController::delete');
