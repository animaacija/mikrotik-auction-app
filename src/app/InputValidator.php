<?php declare(strict_types=1);

/**
 * Class InputValidator validates and typecasts.
 *
 * Rules are arrays of filter-constants keyed by input field key
 * they are applied to. Executed in pipeline.
 *
 * @package Auction
 */
class InputValidator
{
    const REQUIRED = 1;
    const CAST_INT = 2;
    const RULE_STRING_MAX_LEN = 3;
    const RULE_GREATER_THAN = 4;
    const CAST_STRING = 5;
    const CAST_FLOAT = 6;
    const RULE_FLOAT_PRECISION = 7;
    const ARRAY_VALIDATOR = 8;
    const CAST_MULTIPLY = 9;

    /**
     * @var array
     */
    public $rules;
    /**
     * @var string
     */
    public $keyPrefix;
    /**
     * @var array
     */
    protected $fields;

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * InputValidator constructor.
     * @param array $rules
     */
    public function __construct(array $rules = [])
    {
        $this->rules = $rules;
    }

    /**
     * @param int    $typeId
     * @param string $message
     * @param string $key
     */
    private function addError(int $typeId, string $message, string $key)
    {
        $this->errors[] = [
            'id' => $typeId,
            'key' => $key,
            'reason' => $message,
        ];
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array  $fields
     * @param string $prefix
     * @return array
     */
    public function validate(array &$fields, string $prefix = ''): array
    {
        foreach ($this->rules as $fieldKey => $ruleSet) {
            $fieldValue = $fields[$fieldKey] ?? null;

            foreach ($ruleSet as $ruleType => $ruleValue) {
                $errMessage = '';
                switch ($ruleType) {
                    case self::REQUIRED:
                        if (!$fieldValue && $ruleValue) {
                            $errMessage = 'Field is required.';
                        }
                        break;

                    case self::CAST_INT:
                        $fieldValue = $fields[$fieldKey] = intval($fieldValue);
                        break;

                    case self::CAST_FLOAT:
                        $fieldValue = $fields[$fieldKey] = floatval($fieldValue);
                        break;

                    case self::CAST_MULTIPLY:
                        $fieldValue = $fields[$fieldKey] = $fieldValue * $ruleValue;
                        break;

                    case self::CAST_STRING:
                        $fieldValue = $fields[$fieldKey] = strval($fieldValue);
                        break;

                    case self::RULE_STRING_MAX_LEN:
                        if (strlen($fieldValue) > $ruleValue) {
                            $errMessage = "Exceeded {$ruleValue} character limit.";
                        }
                        break;

                    case self::RULE_GREATER_THAN:
                        if ($fieldValue <= $ruleValue) {
                            $errMessage = "Must be grater than {$ruleValue}.";
                        }
                        break;

                    case self::RULE_FLOAT_PRECISION:
                        if (round($fieldValue, $ruleValue) < $fieldValue) {
                            $errMessage = "{$ruleValue} digit precision allowed.";
                        }
                        break;

                    case self::ARRAY_VALIDATOR:
                        if (is_array($fieldValue)) {
                            $validator = new self($ruleValue);
                            foreach ($fieldValue as $index => &$subFields) {
                                $validator->validate($subFields, "{$prefix}{$fieldKey}[{$index}]");
                            }
                            $this->errors = array_merge($this->errors,  $validator->getErrors());
                            $fields[$fieldKey] = $fieldValue;
                        }
                        break;
                }

                if ($errMessage) {
                    if ($prefix) {
                        $fieldKey = "[{$fieldKey}]";
                    }
                    $this->addError($ruleType, $errMessage, $prefix . $fieldKey);
                }
            }
        }

        return $this->errors;
    }
}
