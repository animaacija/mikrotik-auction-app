<?php declare(strict_types=1);

/**
 * Class HtmlRender
 */
class HtmlRender implements RendererInterface
{
    use RendererTrait;

    /**
     * @var string
     */
    public $templatesPath;
    /**
     * @var string
     */
    public $compilationCachePath;
    /**
     * @var string
     */
    public $templatePath;

    /**
     * HtmlRender constructor.
     * @param string $templatePath
     * @param string $templatesPath
     * @param string $compilationCachePath
     */
    public function __construct(
        string $templatePath,
        string $templatesPath,
        string $compilationCachePath
    ) {
        $this->templatesPath = $templatesPath;
        $this->compilationCachePath = $compilationCachePath;
        $this->templatePath = $templatePath;
    }

    /**
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function render(): string
    {
        $loader = new Twig\Loader\FilesystemLoader($this->templatesPath);
        $twig = new Twig\Environment($loader, [
            'cache' => $this->compilationCachePath,
        ]);

        return $twig->render($this->templatePath, $this->getData());
    }
}
