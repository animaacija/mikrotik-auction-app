<?php

return [
    'viewsPath' => BASE_PATH . '/views',
    'cachePath' => BASE_PATH . '/cache',
    'db' => [
        'name' => 'example',
        'user' => 'root',
        'pass' => '',
        'host' => 'db',
    ],
];
