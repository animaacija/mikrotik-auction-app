<?php declare(strict_types=1);

/**
 * Class Response
 */
class Response
{

    /**
     * @var array
     */
    protected $data;
    /**
     * @var array
     */
    protected $headers;
    /**
     * @var int
     */
    protected $code;
    /**
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * Response constructor.
     */
    public function __construct()
    {
        $this->code = 200;
        $this->headers = [];
    }

    /**
     * @param string $location
     * @return Response
     */
    public static function redirect(string $location): Response
    {
        $instance = new self;

        $instance->setCode(302);
        $instance->setHeader('Location', $location);

        return $instance;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function setHeader(string $name, string $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * @param string $path
     * @param array  $data
     * @return Response
     * @throws Exception
     */
    public static function view(string $path, array $data = []): Response
    {
        $instance = new self;

        $renderer = new HtmlRender(
            $path,
            conf('viewsPath'),
            conf('cachePath')
        );

        $renderer->setData($data);
        $instance->setRenderer($renderer);

        return $instance;
    }

    /**
     * @param array $data
     * @return Response
     */
    public static function json(array $data = []): Response
    {
        $instance = new self;
        $renderer = new JsonRender;

        $renderer->setData($data);
        $instance->setRenderer($renderer);
        $instance->setHeader('Content-type', 'application/json');

        return $instance;
    }

    /**
     * @return Response
     * @throws Exception
     */
    public static function notFound(): Response
    {
        $instance = new self;

        if (App::expectsJson()) {
            $renderer = new JsonRender([
                'errors' => [
                    'Resource not found.',
                ],
            ]);
        } else {
            $renderer = new HtmlRender(
                '404.html',
                conf('viewsPath'),
                conf('cachePath')
            );
        }

        $instance->setRenderer($renderer);
        $instance->setCode(404);

        return $instance;
    }

    /**
     * @return RendererInterface
     */
    public function getRenderer(): RendererInterface
    {
        return $this->renderer;
    }

    /**
     * @param RendererInterface $renderer
     */
    public function setRenderer(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function render(): string
    {
        http_response_code($this->code);

        foreach ($this->headers as $name => $value) {
            header("{$name}: {$value}");
        }

        if (!$this->renderer) {
            throw new Exception("Incomplete response object.");
        }

        return $this->renderer->render();
    }
}
