<?php declare(strict_types=1);

/**
 * Trait RendererTrait
 */
trait RendererTrait {

    /**
     * @var array
     */
    protected $data = [];

    /**
     * RendererTrait constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }
}
