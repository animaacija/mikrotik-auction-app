<?php declare(strict_types=1);

/**
 * Class App
 */
class App
{

    /**
     * @var Controller
     */
    protected $controller;
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var
     */
    protected $oldInput;
    /**
     * @var App
     */
    public static $instance;

    /**
     * App constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->controller = new Controller;
        $this->request = $request;

        self::$instance = $this;
    }

    /**
     * @return Request
     */
    public static function getRequest(): Request
    {
        return static::$instance->request;
    }

    /**
     * alias
     *
     * @return bool
     */
    public static function expectsJson(): bool
    {
        return static::getRequest()->getExpectsJSON();
    }

    /**
     * I decided not to implement session,
     * this data is alive at the same request only.
     *
     * @param array $input
     */
    public static function flashInput(array $input)
    {
        self::$instance->oldInput = $input;
    }

    /**
     * @return mixed
     */
    public static function getFlashedInput()
    {
        return self::$instance->oldInput;
    }

    /**
     *
     */
    public static function flushInput()
    {
        self::$instance->oldInput = [];
    }

    /**
     * @return Response
     * @throws Exception
     */
    public function getResponse(): Response
    {
        return $this->controller->processRequest($this->request);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function render(): string
    {
        return $this->getResponse()->render();
    }
}
